import {ethers} from 'ethers';

// from .env
const API_KEY = process.env.API_KEY;
const PRIVATE_KEY = process.env.PRIVATE_KEY;
const CONTRACT_ADDRESS = process.env.OWNER_CONTRACT_ADDRESS;

// create contract instance
const contract = require('../../artifacts/contracts/owner/Owner.sol/Owner.json'); // from compiled contract
const alchemyProvider = new ethers.providers.AlchemyProvider("goerli", API_KEY); // is Alchemy
const signer = new ethers.Wallet(PRIVATE_KEY, alchemyProvider); // is you
const ownerContract = new ethers.Contract(CONTRACT_ADDRESS, contract.abi, signer);

// grab abi
console.log("Contract ABI: " + JSON.stringify(contract.abi));
console.log(`Contract info:
    > contractName: ${contract.contractName}
    > sourceName: ${contract.sourceName}
    > bytecode: ${contract.bytecode}
    > deployedBytecode: ${contract.deployedBytecode}`);

// interact
(async function () {
    console.log("Getting current owner address: ...");
    const owner = await ownerContract.getOwner();
    console.log("Getting current owner address: ... got: " + owner);

    const newOwner = '0xf4A87eB1a6ADafB45b6Fecc4BBA48aa99f0287A4';
    const tx = await ownerContract.changeOwner(newOwner);
    await tx.wait();
    const newOwner2 = await ownerContract.getOwner();

    console.log("Setting new owner address: ...");
    console.log("Setting new owner address: ... done. Set to " + newOwner2);
})();