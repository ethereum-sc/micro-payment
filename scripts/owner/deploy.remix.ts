// Deploy smart contract using ethers.js library
// Please make sure compiled the smart contract file (output to contracts/xxx.sol) before running this script

import {deploy} from 'scripts/ethers-lib';

(async () => {
    try {
        const result = await deploy('Owner', 'owner', []);
        console.log(`Deployed contract, address: ${result.address}`);
    } catch (e) {
        console.log("Deploy smart contract failed. Error: ", e);
    }
})();