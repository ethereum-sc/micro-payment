(async () => {
    try {
        const OwnerFactory = await ethers.getContractFactory('Owner');

        // Start deployment, returning a promise that resolves to a contract object
        const owner = await OwnerFactory.deploy();
        console.log(`Deployed contract, address: ${owner.address}`);
    } catch (e) {
        console.log("Deploy smart contract failed. Error: ", e);
    }
})()
    .then(() => process.exit(0))
    .catch(e => {
        console.log("Deploy smart contract failed. Error: ", e);
        process.exit(1);
    });