import {ethers} from "ethers";

// from .env
const API_KEY = process.env.API_KEY;
const PRIVATE_KEY = process.env.PRIVATE_KEY;
const CONTRACT_ADDRESS = process.env.STORAGE_CONTRACT_ADDRESS;

// create contract instance
const contract = require("../../artifacts/contracts/storage/Storage.sol/Storage.json"); // from compiled contract
const alchemyProvider = new ethers.providers.AlchemyProvider("goerli", API_KEY); // is Alchemy
const signer = new ethers.Wallet(PRIVATE_KEY, alchemyProvider); // is you!
const storageContract = new ethers.Contract(CONTRACT_ADDRESS, contract.abi, signer);

// grab abi
console.log("Contract ABI: " + JSON.stringify(contract.abi));
console.log(`Contract info: 
   > name=${contract.contractName}, 
   > sourceName=${contract.sourceName}, 
   > byteCode=${contract.bytecode}, 
   > deployedBytecode=${contract.deployedBytecode}`);

// interact
(async function () {
    console.log("Getting current Stored value...");
    const val = await storageContract.retrieve();
    console.log("Getting current Stored value... got:", val);

    const newVal = ethers.BigNumber
        .from(val)
        .add(ethers.BigNumber.from(randomIntInRange(1e1, 1e2)));
    console.log(`Storing new value (${newVal})...`);
    const tx = await storageContract.store(newVal);
    await tx.wait();
    const newVal2 = await storageContract.retrieve();
    console.log(`Storing new value (${newVal})... done. New value: ${newVal2}`);
})();

function randomIntInRange(min, max) { // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
}