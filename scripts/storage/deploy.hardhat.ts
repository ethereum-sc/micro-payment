(async () => {
    try {
        const StorageFactory = await ethers.getContractFactory('Storage');

        // Start deployment, returning a promise that resolves to a contract object
        const storage = await StorageFactory.deploy();
        console.log(`Deployed contract, address: ${storage.address}`);
    } catch (e) {
        console.log("Deploy smart contract failed. Error: ", e);
    }
})()
    .then(() => process.exit(0))
    .catch(e => {
        console.log("Deploy smart contract failed. Error: ", e);
        process.exit(1);
    });