// Deploy smart contract using ethers.js library
// Please make sure compiled the smart contract file (output to contracts/xxx.sol) before running this script

import {deploy} from 'scripts/ethers-lib';

var recipientAddress = '0x4B20993Bc481177ec7E8f571ceCaE8A9e22C02db';
var duration = 86400; // in seconds, 86400 for one day

(async () => {
    try {
        const result = await deploy('SimplePaymentChannel', 'micro_payment', [
            recipientAddress, duration
        ]);
        console.log(`Deployed contract, address: ${result.address}`);
    } catch (e) {
        console.log("Deploy smart contract failed. Error: ", e);
    }
})();