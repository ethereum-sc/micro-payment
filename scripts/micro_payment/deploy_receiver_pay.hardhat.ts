(async () => {
    try {
        const ReceiverPaysFactory = await ethers.getContractFactory('ReceiverPays');

        // Start deployment, returning a promise that resolves to a contract object
        const receiverPays = await ReceiverPaysFactory.deploy();
        console.log(`Deployed contract, address: ${receiverPays.address}`);
    } catch (e) {
        console.log("Deploy smart contract failed. Error: ", e);
    }
})()
    .then(() => process.exit(0))
    .catch(e => {
        console.log("Deploy smart contract failed. Error: ", e);
        process.exit(1);
    });