var recipientAddress = '0x4B20993Bc481177ec7E8f571ceCaE8A9e22C02db';
var duration = 86400; // in seconds, 86400 for one day

(async () => {
    try {
        const SimplePaymentChannelFactory = await ethers.getContractFactory('SimplePaymentChannel');

        // Start deployment, returning a promise that resolves to a contract object
        const simplePaymentChannel = await SimplePaymentChannelFactory.deploy(recipientAddress, duration);
        console.log(`Deployed contract, address: ${simplePaymentChannel.address}`);
    } catch (e) {
        console.log("Deploy smart contract failed. Error: ", e);
    }
})()
    .then(() => process.exit(0))
    .catch(e => {
        console.log("Deploy smart contract failed. Error: ", e);
        process.exit(1);
    });