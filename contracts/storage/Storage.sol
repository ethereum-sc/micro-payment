// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.8.2 <0.9.0;

/**
 * @title Storage smart contract
 * @dev Store and Retrieve value in a variable
 */
contract Storage {
    /**
     * Store the value
     */
    uint256 number;

    /**
     * @dev Store value in variable
     * @param num value to store
     */
    function store(uint256 num) public {
        number = num;
    }

    /**
     * @dev Return stored value
     * @return stored value
     */
    function retrieve() public view returns (uint256) {
        return number;
    }
}
