# Welcome to First Friendly with Ethereum smart contract

# PREREQUISITES

We have two ways to start learning with Ethereum smart contract:

1. Remix IDE for coding, compiling/deploying smart contract and running test with pre-built accounts and libraries!
   Download from [here](https://github.com/ethereum/remix-desktop/releases)
2. Build yourself with hardhat and some ethereum libraries... Run:

```
npm install
npx hardhat compile
npx hardhat run scripts/xxx --network yyy
```

### 1. Remix IDE

Run Remix IDE, open this project folder in Remix. Note: if Remix does not update, close and re-run Remix again to see
the project files get loaded

Right click on `contracts/xxx/yyy.sol` > click `compile` to compile the selected smart contract, see:

<img alt="compile" src="https://img001.prntscr.com/file/img001/j60vGPPmTOCF-YF3BYWpEA.png" height="250">

Right click on `scrips/xxx/yyy.remix.ts` > click `run` to run script deployment contract to test environment, see:

<img alt="compile" src="https://img001.prntscr.com/file/img001/EAMHVMGPR8S51Lbd5BGa2A.png" height="250">

From Remix left navigator, click bottom icon (`Deploy and run transactions`) and we can interact with contracts via some
public apis of them, see:

<img alt="compile" src="https://img001.prntscr.com/file/img001/G81IoxPETTuo0-3upU-NdQ.png" height="350">

### Build yourself with hardhat and some ethereum libraries

**Reference:** 
- [1. Create and deploy smart contract](https://ethereum.org/vi/developers/tutorials/hello-world-smart-contract)
  - Steps from 1 to 16
  - Become User Friendly with Ethereum network, Goerli (testnet) for Ethereum, Metamask, Alchemy, Hardhat plugin
  - Create smart contract
  - Deploy smart contract to testnet
- [2. Interacting with a Smart Contract](https://docs.alchemy.com/docs/interacting-with-a-smart-contract)
  - Step 17
  - Interact (call) with (functions of) smart contract
- [3. Submitting your Smart Contract to Etherscan](https://docs.alchemy.com/docs/submitting-your-smart-contract-to-etherscan)
  - Steps from 18 to the end
  - Public smart contract to Ethereum `Ether-scan` (mainnet)

There are many ways to make requests to the Ethereum chain. For simplicity, we'll use a free account on Alchemy, a
blockchain developer platform and API that allows us to communicate with the Ethereum chain without having to run our
own nodes.

The platform also has developer tools for monitoring and analytics that we'll take advantage of in this tutorial to
understand what's going on under the hood in our smart contract deployment.

#### 1. Connect to Ethereum network

Signup and login from [Alchemy](https://dashboard.alchemyapi.io/signup)

#### 2. Create your app (and API key)

From Alchemy, do `Create App`, enter name, choose `Ethereum` for chain and `Goerli Testnet` for network.

<img alt="Alchemy" src="https://img001.prntscr.com/file/img001/oRxftw4IRyiHKvkdNRaUwg.png" height="150">

#### 3. Create an Ethereum account (address)

We need an account to send/receive transactions. We'll use MetaMask wallet, a virtual wallet in the browser to manage
our Ethereum account address and balance, it's very popular usage in Ethereum network!

Download and install MetaMask from [here](https://metamask.io/download/), used as a browser extension. Open MetaMask and
create new account in it, choose network `Goerli Test Network` for testing only, that comes with faucet money, no
required real money!

<img alt="MetaMask" src="https://ethereum.org/static/8e94518c4504eb3b622ad0f1111d00ca/37e0d/metamask-ropsten-example.png" height="250">

#### 4. Add Ether from a Faucet

Request ETH from [Goerli Faucet](https://goerlifaucet.com/)

- Login using Alchemy account
- Enter account address that be got from MetaMask wallet
- Click `Send Me ETH`, wait and enjoy!

#### 5. Check your balance

Open MetaMask and check if account ETH balance be increased!

#### 6. Init project

Create project directory and run `npm init` to first init project. This step is for first creating from blank project,
it's already run and no need to run again.

#### 7. Download Hardhat

Hardhat is development environment to compile, deploy, test and debug our Ethereum software. It helps developers when
building Smart Contracts and Dapps locally before deploying to the live chain.

This step is for first creating from blank project, it's already run, save to package.json and no need to run again. Now
run `npm install` instead.

#### 8. Create Hardhat project

Run `npx hardhat` for creating new project. Select "create an empty hardhat.config.js". This step is for first creating
from blank project, it's already run.

#### 9. Add project folders

Folders: contracts, scripts, tests

#### 10. Write contracts

View existing source code!

#### 11. Connect MetaMask and Alchemy to the project

For first creating project only:

```
npm install dotenv --save

vi .env
# content of .env like this:
API_URL = "https://eth-goerli.alchemyapi.io/v2/your-api-key"
PRIVATE_KEY = "your-metamask-private-key"
```

Please enter valid value for API_URL and PRIVATE_KEY in .env, where:

- `your-api-key` got from Alchemy app information. Steps:
    - Login to Alchemy > click Dashboard > Click `Micro-Payment` app
    - Click `VIEW KEY` and copy the API KEY value like this `_ct35clBERPksJW_626Za0vF8vYQeACW`
- `your-metamask-private-key` got from MetaMask. Steps:
    - Open and login MetaMask, click on account, choose `Account details`
    - Click `Export private key` > enter your password > copy private key like
      this `4d63f13fe866f61d822cd8edbd1a840b2e87b0adeb12eb1c09e447cf935a3227`

#### 12. Install ethers.js

Ethers.js is a library that makes it easier to interact and make requests to Ethereum by
wrapping `standard JSON-RPC methods` with more user-friendly methods.

Hardhat makes it super easy to integrate [Plugins](https://hardhat.org/hardhat-runner/plugins) for additional tooling
and extended functionality. We'll be taking advantage of
the [Ethers Plugin](https://hardhat.org/hardhat-runner/plugins/nomiclabs-hardhat-ethers) for contract deployment.

It's already run with command and no need run again:

```
npm install --save-dev @nomiclabs/hardhat-ethers "ethers@^5.0.0"
```

#### 13. Update Hardhat config

Update the hardhat.config.js for solidity version, running network, API_URL, PRIVATE_KEY and more... e.g. like this:

```
require('dotenv').config();

require("@nomiclabs/hardhat-ethers");
const { API_URL, PRIVATE_KEY } = process.env;

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: "0.8.17",
  defaultNetwork: "goerli",
  networks: {
    hardhat: {},
    goerli: {
      url: API_URL,
      accounts: [`0x${PRIVATE_KEY}`]
    }
  },
}
```

#### 14. Compile smart contract

Run this command to compile:

```
npx hardhat compile
```

and the output will be put in `./artifacts` folder with `artifacts/contracts` structure likes the contracts source code,
that contains smart contract `.json` files.

#### 15. Write deploy script

The `ContractFactory` in `ethers.js` is an abstraction used to deploy new smart contracts, e.g. the `StorageFactory` is
a factory for instances of Storage contract, the `storage` contract instance. Calling `deploy()` on a ContractFactory
will start the deployment, and return a Promise that resolves to a Contract. We use `hardhat-ethers` plugin, then the
`ContractFactory` and `Contract` instances are connected to the first signer by default.

The code is already in `deploy.hardhat.ts`.

#### 16. Deploy smart contract

Run this command:

```
npx hardhat run scripts/xxx/deploy.hardhat.js --network goerli
```

Then we will get something like (wih `address` is different from each deployment):

```
Deployed contract, address: 0xFf1f2674949f1b251560DBF2c8A5Bf5523d4620d
```

Go to [goerli ether scan](https://goerli.etherscan.io/) page, enter deployed contract address and we can see its detail
like:

<img alt="deployed-contract" src="https://img001.prntscr.com/file/img001/6t4vFYM-SYO8EX1IBfQfzg.png" height="150">

Click on Txn Hash value to see more detail like this:

<img alt="deployed-contract2" src="https://img001.prntscr.com/file/img001/cipunq17T3GkbkXT2rFIHQ.png" height="400">

#### 17. Interact with smart contract

Update .env file for `CONTRACT_ADDRESS` key like this:

```
API_URL = "https://eth-goerli.g.alchemy.com/v2/<your-api-key>"
API_KEY = "<your-api-key>"
PRIVATE_KEY = "<your-metamask-private-key>"
CONTRACT_ADDRESS = "0x<your contract address>"
```

Create an interact.js file includes some stuffs:
- const from .env
```
const API_KEY = process.env.API_KEY;
const PRIVATE_KEY = process.env.PRIVATE_KEY;
const CONTRACT_ADDRESS = process.env.CONTRACT_ADDRESS;
```
- grab contract abi
- create contract instance
- interact with contract

# MICRO PAYMENT

In this section, we will learn how to build an example implementation of a payment channel. It uses cryptographic
signatures to make repeated transfers of Ether between the same parties secure, instantaneous, and without transaction
fees. For the example, we need to understand how to sign and verify signatures, and set up the payment channel.

## Creating and verifying signatures

Imagine Alice wants to send some ETH to Bob, i.e. Alice is the sender and Bob is the recipient.

Alice only needs to send cryptographically signed message off-chain (e.g. via email) to Bob, and it is similar to
writing checks.

Alice and Bob use signatures to authorize transactions, which is possible with smart contracts on Ethereum. Alice will
build a simple smart contract that lets her transmit ETH, but instead of calling a function herself to initiate a
payment, she will let Bob do that, and therefore pay the transaction fee.

The contract will work as follows:

1. Alice deploys the `ReceiverPays` contract, attaching enough ETH to cover the payments that will be made
2. Alice authorizes a payment by signing a message with her private key
3. Alice sends the cryptographically signed message to Bob. The message does not need to be kept secret
   (explained later!), and the mechanism for sending it does not matter
4. Bob claims his payment by presenting the signed message to the smart contract, it verifies the authenticity of the
   message and the releases the funds

### Creating the signature

Alice does not need to interact with the Ethereum network to sign the transaction, the process is completely offline. We
will sign messages in the browser using web3.js and MetaMask wallet, using the method described in EIP-712, as it
provides a number of other security benefits.

... more info ...

### What to Sign

For a contract that fulfils payments, the signed message must include:

1. The recipient's address
2. The amount to be transferred
3. Protection against replay attacks

A replay attack is when a signed message is reused to claim authorization for a second action. To avoid replay attacks
we use the same technique as in Ethereum transactions themselves, a so-called nonce, which is the number of transactions
sent by an account. The smart contract checks if a nonce is used multiple times.

Another type of replay attack can occur when the owner deploys a `ReceiverPays` smart contract, makes some payments, and
then destroys the contract. Later, they decide to deploy the `RecipientPays` smart contract again, but the new contract
does not know the nonces used in the previous deployment, so the attacker can use the old messages again.

Alice can protect against this attack by including the contract's address in the message, and only messages containing
the contract's address itself will be accepted. See first two lines of the `claimPayment()` function in source code.

### Packing arguments

Now that we have identified what information to include in the singed message, we are ready to put the message together,
hash it and sign it. For simplicity, we concatenate the data. The ethereumjs-abi library provides a function
called `soliditySHA3` that mimics (bắt chước) the behaviour of Solidity's `keccak256` function applied to arguments
encoded using `abi.encodePacked`.

### Recovering the Message Signer in Solidity

In general, ECDSA signatures consist of two parameters, `r` and `s`. Signatures in Ethereum include a third parameter
called `v`, that you can use to verify which account's private key was used to sign the message, and the transaction's
sender.

Solidity provides a built-in function ecrecover that accepts a message along with the `r`, `s` and `v` parameters and
returns the address that was used to sign the message.

### Extracting the Signature Parameters

Signatures produced by web3.js are the concatenation of r, s and v, so the first step is to split these parameters
apart. You can do this on the client-side, but doing it inside the smart contract means you only need to send one
signature parameter rather tha three.

Splitting apart a byte array into its constituent (thành phần) parts is a mess, so we inline assembly to do the job in
the
`splitSignature` function (view at the end of source code).

### Computing the Message Hash

The smart contract needs to know exactly what parameters were signed, and so it must recreate the message from the
parameters and use that for signature verification. The functions `prefixed` and `recoverSigner` do this in
the `claimPayment` function.

Now please view full smart contract from source code!

## Writing a Simple Payment Channel

Alice now builds a simple but complete implementation of a payment channel. Payment channels use cruptographic
signatures to make repeated transfers of Ether securely, instantaneously, and without transaction fees.

### What is a Payment Channel?

Payment channels allow participants to make repeated transfers of Ether without using transactions. This means that you
can avoid the delays and fees associated with transactions. We are going to explore a simple unidirectional payment
channel between two parties (Alice ad Bob). It involves three steps:

1. Alice funds (quỹ, tiền vốn) a smart contract with Ether. This "opens" the payment channel
2. Alice signs messages that specify how much of that Ether is owned to the recipient. This step is repeated for each
   payment
3. Bob "closes" the payment channel, withdrawing his portion of the Ether and sending the remainder back to the sender

Notice: only steps 1 and 3 require Ethereum transactions, step 2 means that the sender transmits a cryptographically
signed message to the recipient via off chain methods (e.g. email). This means only two transactions are required to
support any number of transfers.

Bob is guaranteed (đảm bảo) to receive his funds because the smart contract escrows (ký quỹ) the Ether and honours (danh
dự) a valid signed message. The smart contract also enforces a timeout, so Alice is guaranteed to eventually recover her
funds even if the recipient refuses to close the channel. It is up to the participants in a payment channel to decide
how long to keep it open. For a short-lived transactions, such as paying an internet cafe for each minute of network
access, the payment channel may be kept open for a limited duration. On the other hand, for a recurring payment, such as
paying an emplyee an hourly wage (tiền công), the payment channel may be kept open for several months or years.

### Opening the Payment Channel

To open the payment channel, Alice deploys the smart contract, attaching the Ether to be escrowed and specifying the
intended (dự định) recipient and a maximum duration for the channel to exist. This is the function SimplePaymentChannel
in the contract, please view in the source code.

### Making Payments

Alice makes payments by sending signed message to Bob. This step is performed entirely outside the Ethereum network.
Messages are cryptographically signed by the sender and then transmitted directly to the recipient.

Each message includes the following information:

- The smart contract's address, used to prevent cross-contract replay attecks
- The total amount of Ether that is owed to the recipient so far

... need re-reed ... A payment channel is closed just once, at the end of a series of transfers. Because of this, only
one of message sent is redeemed (chuộc lại). This is why each message specifies a cumulative total amount of Ether owed,
rather than the amount of the individual micro-payment. The recipient will natureally choose to redeem the most recent
message because that is the one with the higest total. The nonce per-message is not needed anymore, because the smart
contract only honours a single message. The address of the smart contract is still used to prevent a message intended
for one payment channel from being used for a different channel. ... ./end need re-reed ...

### Closing the Payment Channel

When Bob is ready to receive his funds, it is time to close the payment channel by calling a `close` function on the
smart contract. Closing the channel pays the recipient the Ether they are owed and destroys the contract, sending any
remaining Ether back to Alice. To close the channel, Bob needs to provide a message signed by Alice.

The smart contract must verify that the message contains a valid signature from the sender. The process for doing this
verification is the same as the process the recipient uses. The Solidity functions `isValidSignature`
and `recoverSigner`
work just like their JavaScript counterparts in the previous section, with the latter function borrowed from the
ReceiverPays contract.

Only the payment channel recipient can call the close function, who naturally passes the most payment message because
that message carries the highest total owed. If the sender were allowed to call this function, they could provide a
message with a lower amount and cheat the recipient out of what they are owed.

The function verifies the signed message matches the given parameters. If everything checks out, the recipient is sent
their portion of the Ether, and the sender is sent the rest via a selfdestruct. See the `close` function in the full
source code.

### Verifying Payments

Unlike Making Payments, message in a payment channel are not redeemed right away. The recipient keeps track of latest
message and redeem it when it's time to close the payment channel. This means it's critical that the recipient perform
their own veification of each message. Otherwise there is no guarantee that the recipient will be able to get paid in
the end.

---

# Owner Contract

A simplest smart contract to get and set current contract owner!
Use `hardhat/console.sol` for writing to log as with function `console.log()` similar to javascript.

### Init owner on deployment

Set current `sender` as contract owner

### Change Owner

Verify if the `sender` is current owner, if passed, set the sender as new owner

### Get Owner

Return current contract owner

---

# Simple Storage Number

A simplest smart contract to get/set anonymously a number to contract!

Use an `uint256 number` to store value and return for client when it calls `retrieve()` method.
